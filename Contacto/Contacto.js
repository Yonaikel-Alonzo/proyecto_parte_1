// Obtener el formulario y el botón de enviar
const form = document.querySelector('form');
const button = document.querySelector('button[type="submit"]');

// Agregar un evento de clic al botón de enviar
button.addEventListener('click', function(event) {
  // Detener el envío del formulario por defecto
  event.preventDefault();

  // Obtener los valores de los campos de entrada
  const name = form.querySelector('input[type="text"]').value;
  const email = form.querySelector('input[type="email"]').value;
  const subject = form.querySelector('input[type="text"]').value;
  const message = form.querySelector('textarea').value;

  // Validar los campos de entrada
  if (name === '') {
    alert('Ingrese su nombre y apellido');
    return;
  }

  if (!email.includes("@")) {
    alert('Ingrese un Correo valido');
    return;
  }

  if (subject === '') {
    alert('Ingrese el asunto de su mensaje');
    return;
  }

  if (message === '') {
    alert('Ingrese su mensaje');
    return;
  }

  // Enviar el formulario si todos los campos son válidos
  form.submit();
  alert('¡Gracias por contactarnos! Nos pondremos en contacto contigo pronto.');
  window.location="Inicio.html";
});

	
