function consultarBeca() {
    // Obtiene el ID de la beca
    var id_beca = document.getElementById("id_beca").value;
    // Lógica para realizar la consulta de la beca
    // En este ejemplo, se asume que la información de la beca
    // está almacenada en un arreglo de objetos
    var becas = [
        {
            id: 1,
            estado: "Pendiente",
            fecha_aprobacion: "-"
        },
        {
            id: 2,
            estado: "Pendiente",
            fecha_aprobacion: "-"
        },
        {
            id: 3,
            estado: "Aprobada",
            fecha_aprobacion: "20/08/2023"
        },
        {
            id: 4,
            estado: "Rechazada",
            fecha_aprobacion: "01/10/2023"
        }
    ];
    // Busca la beca en el arreglo de becas
    var beca_encontrada = becas.find(function(beca) {
        return beca.id == id_beca;
    });
    // Actualiza la tabla con la información de la beca
    var tabla_beca = document.getElementById("tabla_beca");
    var tbody_beca = tabla_beca.getElementsByTagName("tbody")[0];
    if (beca_encontrada) {
        // Si se encuentra la beca, muestra su información en la tabla
        tbody_beca.innerHTML = "<tr><td>" + beca_encontrada.id + "</td><td>" + beca_encontrada.estado + "</td><td>" + beca_encontrada.fecha_aprobacion + "</td></tr>";
    } else {
        // Si no se encuentra la beca, muestra un mensaje de error en la tabla
        tbody_beca.innerHTML = "<tr><td colspan='3'>No se encontró la beca con ID " + id_beca + "</td></tr>";
    }
}
function actualizarEstadoBeca() {
    // Obtiene el ID de la beca y el nuevo estado
    var id_beca = document.getElementById("id_beca").value;
    var nuevo_estado = document.getElementById("nuevo_estado").value;
    // Lógica para actualizar el estado de la beca
    // En este ejemplo, se asume que la información de la beca
    // está almacenada en un arreglo de objetos
    var becas = [
        {
            id: 1,
            estado: "Pendiente",
            fecha_aprobacion: "-"
        },
        {
            id: 2,
            estado: "Aprobada",
            fecha_aprobacion: "03/05/2023"
        },
        {
            id: 3,
            estado: "Rechazada",
            fecha_aprobacion: "01/05/2023"
        }
    ];
    // Busca la beca en el arreglo de becas
    var beca_encontrada = becas.find(function(beca) {
        return beca.id == id_beca;
    });
  
    // Actualiza el estado de la beca
    if (beca_encontrada) {
        beca_encontrada.estado = nuevo_estado;
        beca_encontrada.fecha_aprobacion = new Date().toLocaleDateString();
  
           // Actualiza la tabla con la información de la beca
    var tabla_beca = document.getElementById("tabla_beca");
    var tbody_beca = tabla_beca.getElementsByTagName("tbody")[0];
    tbody_beca.innerHTML = "<tr><td>" + beca_encontrada.id + "</td><td>" + beca_encontrada.estado + "</td><td>" + beca_encontrada.fecha_aprobacion + "</td></tr>";

    // Muestra un mensaje de éxito
    alert("Estado de la beca actualizado con éxito");
} else {
    // Si no se encuentra la beca, muestra un mensaje de error
    alert("No se encontró la beca con ID " + id_beca);
}
}
function Regresar(){
    window.location="Obten_Beca.html";
}
