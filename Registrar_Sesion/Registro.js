const formulario = document.querySelector('#Registro');

function Registrarse() {
    const Name = document.getElementById("name").value;
    const apell = document.getElementById("apell").value;
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const dni = document.getElementById("dni").value;
    const Na = document.getElementById("Na").value;
    const cel = document.getElementById("cel").value;
    const estudio = document.getElementById("estudio").value;

    if (Name === "" || apell === "" || email === "" || password === "" || dni === "" || Na === "" || cel === "" || estudio === "") {
        document.getElementById("mensaje-error").innerHTML = "Por favor complete todos los campos.";
        alert("Complete todos los campos");
        return false;
    }

    if (dni.length !== 10) {
        document.getElementById("mensaje-error").innerHTML = "Por favor Ingreser la cedula correcta.";
        alert("Por favor Ingreser la cedula correcta..");
        return false;
    }

    if (!email.includes("@live.uleam.edu.ec")) {
        document.getElementById("mensaje-error").innerHTML = "El correo debe ser institucional (@live.uleam.edu.ec).";
        alert("Debe ser un correo institucional");
        return false;
    }

    alert("Registro Exitoso");
    window.location = "Inicio.html";
}
