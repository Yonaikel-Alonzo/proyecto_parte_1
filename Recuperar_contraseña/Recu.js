function Recupera(event) {
    // Prevenir que el formulario se envíe automáticamente
    event.preventDefault();
    // Obtener el valor del campo de correo electrónico
    var email = document.getElementById("email").value;
    // Validar el campo de correo electrónico
    var email_regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (!email_regex.test(email)) {
      alert("Por favor ingrese un correo electrónico válido");
      return false;
    }
    else
    // Si el campo de correo electrónico es válido, enviar el formulario
    alert("Correo válido. Formulario enviado.");
    window.location.href = "Iniciar_Sesion.html";
    return true;
}
  