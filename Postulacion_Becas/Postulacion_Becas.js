const form = document.querySelector('#form-becas');
form.addEventListener('submit', function (event) {
  event.preventDefault();
  enviarFormulario();
});
function enviarFormulario() {
  const nombre = document.getElementById('nombre').value;
  const edad = document.getElementById('edad').value;
  const email = document.getElementById('email').value;
  const telefono = document.getElementById('telefono').value;
  const carrera = document.getElementById('carrera').value;
  const Unidad = document.getElementById('Unidad').value;
  const ingresos = document.getElementById('ingresos').value;
  const motivacion = document.getElementById('motivacion').value;
  // Validar que los campos no estén vacíos
  if (nombre === '' || edad === '' || email === '' || telefono === '' || carrera === '' || Unidad === '' || ingresos === '' || motivacion === '') {
    alert('Por favor, rellene todos los campos.');
    return false;
  }
  // Validar que la edad y los ingresos sean números positivos
  if (isNaN(edad) || isNaN(ingresos) || edad <= 0 || ingresos <= 0) {
    alert('Por favor, introduzca una edad e ingresos válidos.');
    return false;
  }
  // Validar que el email tenga un formato válido
  const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!regexEmail.test(email)) {
    alert('Por favor, introduzca un email válido.');
    return false;
  }
  alert('Formulario enviado con éxito.');
  window.location = "Inicio.html";
}
function Regresar(){
  window.location="Obten_Beca.html";
}






